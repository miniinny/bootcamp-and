package tw.bootcamp.helloworld.ui.details;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import tw.bootcamp.helloworld.model.Pokemon;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DetailsPresenterTest {
    @Mock
    DetailsView detailsView;

    DetailsPresenter detailsPresenter;

    @Before
    public void setUp() throws Exception {
        detailsPresenter = new DetailsPresenter(detailsView);
    }

    @Test
    public void shouldShowPokemonDetailsOnCreate() throws Exception {
        detailsPresenter.onCreate(new Pokemon("name", null, null, null, null, 1));

        verify(detailsView).showPokemonDetails("name", null, null, null, null);
    }

    @Test
    public void shouldSetTitleDetailsOnCreate() throws Exception {
        detailsPresenter.onCreate(new Pokemon("name", null, null, null, null, 1));

        verify(detailsView).showTitle("name");
    }
}