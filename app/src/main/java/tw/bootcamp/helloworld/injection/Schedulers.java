package tw.bootcamp.helloworld.injection;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

import javax.inject.Inject;


public class Schedulers {
    @Inject
    public Schedulers() {
    }

    public Scheduler io() {
        return rx.schedulers.Schedulers.io();
    }

    public Scheduler main() {
        return AndroidSchedulers.mainThread();
    }
}
