package tw.bootcamp.helloworld.injection;

import dagger.Module;
import dagger.Provides;
import tw.bootcamp.helloworld.service.PokemonService;

@Module
public class AppModule {
    @Provides
    PokemonService providePokemonService() {
        return new PokemonService();
    }
}
