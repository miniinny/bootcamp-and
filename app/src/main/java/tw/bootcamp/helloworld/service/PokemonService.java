package tw.bootcamp.helloworld.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import tw.bootcamp.helloworld.BuildConfig;
import tw.bootcamp.helloworld.model.Pokemon;
import tw.bootcamp.helloworld.model.PokemonList;

import java.util.List;

public class PokemonService {
    private static final String BASE_URL = "https://raw.githubusercontent.com/";
    private final PokemonServiceEndpoint pokemonServiceEndpoint;

    public PokemonService() {
        Gson gson = new GsonBuilder().create();
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(loggingInterceptor);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .client(clientBuilder.build())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();

        pokemonServiceEndpoint = retrofit.create(PokemonServiceEndpoint.class);
    }

    public Observable<List<Pokemon>> getPokemontList() {
        return pokemonServiceEndpoint.getPokemonList()
                .map(new Func1<PokemonList, List<Pokemon>>() {
                    @Override
                    public List<Pokemon> call(PokemonList pokemonList) {
                        return pokemonList.getPokemonList();
                    }
                });
//        return pokemonServiceEndpoint.getPokemonList()
//                .flatMap(new Func1<PokemonList, Observable<List<Pokemon>>>() {
//                    @Override
//                    public Observable<List<Pokemon>> call(PokemonList pokemonList) {
//                        return Observable.just(pokemonList.getPokemonList());
//                    }
//                });
    }
}
