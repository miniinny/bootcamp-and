package tw.bootcamp.helloworld.service;

import retrofit2.http.GET;
import rx.Observable;
import tw.bootcamp.helloworld.model.PokemonList;

public interface PokemonServiceEndpoint {
    @GET("Biuni/PokemonGO-Pokedex/master/pokedex.json")
    Observable<PokemonList> getPokemonList();
}
