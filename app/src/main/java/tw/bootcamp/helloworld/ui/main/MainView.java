package tw.bootcamp.helloworld.ui.main;

import tw.bootcamp.helloworld.model.Pokemon;

import java.util.List;

public interface MainView {
    void showToastMessage(String message);
    void updateTable(List<Pokemon> pokemonList);
}
