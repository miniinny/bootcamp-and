package tw.bootcamp.helloworld.ui.details;

public interface DetailsView {
    void showPokemonDetails(String name, String weight, String height, String candy, String url);
    void showTitle(String title);
}
