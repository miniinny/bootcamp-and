package tw.bootcamp.helloworld.ui.details;

import tw.bootcamp.helloworld.model.Pokemon;

import java.lang.ref.WeakReference;

public class DetailsPresenter {
    private WeakReference<DetailsView> detailsView;

    public DetailsPresenter(DetailsView detailsView) {
        this.detailsView = new WeakReference(detailsView);
    }

    public void onCreate(Pokemon pokemon) {
        detailsView.get().showPokemonDetails(pokemon.getName(),
                pokemon.getWeight(),
                pokemon.getHeight(),
                pokemon.getCandy(),
                pokemon.getImg());
        detailsView.get().showTitle(pokemon.getName());
    }
}
