package tw.bootcamp.helloworld.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import tw.bootcamp.helloworld.R;
import tw.bootcamp.helloworld.injection.PokemonApplication;
import tw.bootcamp.helloworld.model.Pokemon;
import tw.bootcamp.helloworld.ui.details.DetailsActivity;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {

    public static final String POKEMON_EXTRA = "pokemonExtra";

    @Inject
    MainPresenter mainPresenter;

    private PokedexAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.main_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PokedexAdapter();
        recyclerView.setAdapter(adapter);

        PokemonApplication.getAppComponent().inject(this);
        mainPresenter.attachView(this);

        mainPresenter.onCreate();

    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateTable(List<Pokemon> pokemonList) {
        adapter.updateData(pokemonList);
    }

    public class PokedexAdapter extends RecyclerView.Adapter<PokedexItemViewHolder> {
//        List<Pokemon> pokemonList = Arrays.asList(new Pokemon("Pikachu", "10kg", "something", "15cm", "http://vignette4.wikia.nocookie.net/pokemon/images/5/5f/025Pikachu_OS_anime_11.png/revision/latest?cb=20150717063951", R.drawable.pokemon25));
        List<Pokemon> pokemonList = new ArrayList<>();

        @Override
        public PokedexItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
            return new PokedexItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(PokedexItemViewHolder holder, int position) {
            holder.showItem(pokemonList.get(position));
        }

        @Override
        public int getItemCount() {
            return pokemonList.size();
        }

        public void updateData(List<Pokemon> pokemonList) {
            this.pokemonList = pokemonList;
            notifyDataSetChanged();
        }
    }

    public class PokedexItemViewHolder extends RecyclerView.ViewHolder {

        private final TextView subtitle;
        private final TextView title;
        private final ImageView icon;

        public PokedexItemViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.item_title);
            subtitle = (TextView) itemView.findViewById(R.id.item_subtitle);
            icon = (ImageView) itemView.findViewById(R.id.item_icon);
        }

        public void showItem(final Pokemon pokemon) {
            title.setText(pokemon.getName());
            subtitle.setText(pokemon.getHeight());
            Picasso.with(itemView.getContext()).load(pokemon.getImg()).into(icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                    intent.putExtra(POKEMON_EXTRA, pokemon);
                    startActivity(intent);
                }
            });
        }
    }
}