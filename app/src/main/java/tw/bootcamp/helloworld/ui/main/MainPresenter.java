package tw.bootcamp.helloworld.ui.main;

import rx.Subscriber;
import tw.bootcamp.helloworld.model.Pokemon;
import tw.bootcamp.helloworld.service.PokemonService;

import javax.inject.Inject;
import java.lang.ref.WeakReference;
import java.util.List;

public class MainPresenter {
    private final PokemonService pokemonService;
    private final tw.bootcamp.helloworld.injection.Schedulers schedulers;

    private WeakReference<MainView> mainViewWeakReference;

    @Inject
    public MainPresenter(PokemonService pokemonService, tw.bootcamp.helloworld.injection.Schedulers schedulers) {
        this.pokemonService = pokemonService;
        this.schedulers = schedulers;
    }

    public void attachView(MainView mainView) {
        mainViewWeakReference = new WeakReference<>(mainView);
    }

    public void onCreate() {
        pokemonService.getPokemontList()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.main())
                .subscribe(new Subscriber<List<Pokemon>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        mainViewWeakReference.get().showToastMessage(e.getMessage());
                    }

                    @Override
                    public void onNext(List<Pokemon> pokemons) {
                        mainViewWeakReference.get().updateTable(pokemons);
                    }
                });
    }
}
