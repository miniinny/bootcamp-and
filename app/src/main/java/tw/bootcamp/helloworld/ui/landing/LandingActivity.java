package tw.bootcamp.helloworld.ui.landing;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import tw.bootcamp.helloworld.ui.main.MainActivity;
import tw.bootcamp.helloworld.R;

public class LandingActivity extends AppCompatActivity {

    private static final long SPLASH_DELAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        startSplash();
    }

    private void startSplash() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    startNextActivity();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }, SPLASH_DELAY_LENGTH);
    }

    private void startNextActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
        this.finish();
    }
}
