package tw.bootcamp.helloworld.model;

import android.support.annotation.DrawableRes;

import java.io.Serializable;

public class Pokemon implements Serializable {
    private String name;
    private String height;
    private String weight;
    private String candy;
    private String img;
    @DrawableRes private int drawableId;

    public Pokemon(String name, String weight, String candy, String height, String img, @DrawableRes int drawableId) {
        this.name = name;
        this.height = height;
        this.weight = weight;
        this.candy = candy;
        this.img = img;
        this.drawableId = drawableId;
    }

    public String getName() {
        return name;
    }

    public String getHeight() {
        return height;
    }

    public String getWeight() {
        return weight;
    }

    public String getCandy() {
        return candy;
    }

    public String getImg() {
        return img;
    }

    @DrawableRes
    public int getDrawableId() {
        return drawableId;
    }
}
